package com.company.pieces;

public abstract class ChessPiece {

    private int value;
    private int x;
    private int y;

    public abstract boolean makeMove(int endX, int endY);



}
